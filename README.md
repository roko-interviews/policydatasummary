# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Willis Tower Watson Technical Test.

### How do I get set up? ###

* Summary of set up
	* Download the repository.
	* Unzip the source code.
	* Navigate to "\policydatasummary\WillisTowersWatson" folder and open "WillisTowersWatson.sln" in Visual Studio (2019 has been used for development).
	* Set the ConsoleApp project as "Start Up" project.
	* Configure ConsoleApp project settings to set up the application arguments (Example- "C:\\test\\Input.txt" "C:\\test\\Output.csv").
	* Hit the run/play button in Visual studio. (Alternatively you can compile the program and run it from command prompt by passing the necessary arguments, i,e Input and Output file names).
	* Console will display each succesful/failed processing of input data from input file. Everything displayed in the console can also be found in the log files in executing directory of the program.
	* Output of the program will be available in the file that you had passed as program args at the start of the execution.
	* Some unit tests have been supplied. You can run them from VS/ any preffered test harnness.
	
### Limitations ###

* No tests has been done to check, how this code will cope with a very large size of input file with thousands//millions of records.
* Error handling could have been improved. Usually I prefer using Postsharp to take all the error/exception handling code out of the class logic, to keep the code logic clean. 
	But for this excercise it felt a overkill to me to integrate and configure Postsharp.
* Initially I was told, it will be a simple technical test taking only 1-2 hours. But Understanding the problem + planning + implementations + error handling + manual tests + unit test + code comment/document etc, it is nearly a day worth of effort easily. So I have tried to cut corners here and there.

### Contribution guidelines ###


### Who do I talk to? ###

* Repo owner or admin. tuhiin@gmail.com
* Other community or team contact