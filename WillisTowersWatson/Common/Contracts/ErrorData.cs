﻿using System;

namespace Common.Contracts
{
    public class ErrorData
    {
        /// <summary>
        /// Gets or sets Error Code
        /// </summary>
        public string ErrorCode { get; set; }

        /// <summary>
        /// Gets or sets InnerException
        /// </summary>
        public Exception InnerException { get; set; }
    }
}
