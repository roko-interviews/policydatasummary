﻿namespace Common.ServiceInterfaces
{
    public interface IDataExporter<T>
    {
        /// <summary>
        /// Exports data.
        /// </summary>
        /// <param name="data"></param>
        void Export(T data);
    }
}
