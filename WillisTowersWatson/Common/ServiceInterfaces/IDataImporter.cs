﻿namespace Common.ServiceInterfaces
{
    public interface IDataImporter<T>
    {
        /// <summary>
        /// Imports data
        /// </summary>
        /// <returns>Data</returns>
        T Import();
    }
}
