﻿namespace Common.ServiceInterfaces
{
    public interface IExporter
    {
        /// <summary>
        /// Exports to external system.
        /// </summary>
        void ExportToExternalSystem(string policySummaryCsvContent);

    }
}
