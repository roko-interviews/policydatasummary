﻿namespace Common.ServiceInterfaces
{
    public interface IImporter
    {
        /// <summary>
        /// Imports from external system.
        /// </summary>
        /// <returns></returns>
        string[] ImportFromExternalSystem();
    }
}
