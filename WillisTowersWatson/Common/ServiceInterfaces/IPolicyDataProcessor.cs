﻿namespace Common.ServiceInterfaces
{
    public interface IPolicyDataProcessor
    {
        /// <summary>
        /// Processes policy data.
        /// </summary>
        void Run();
    }
}
