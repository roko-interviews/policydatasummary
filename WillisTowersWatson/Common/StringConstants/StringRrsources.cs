﻿namespace Common.StringConstants
{
    /// <summary>
    /// Improvement: This string content can be moved to a .net resource file.
    /// </summary>
    public class StringRrsources
    {
        // Validation errors
        public const string VALIDATION_ERROR_POLICY_ID_MUST_BE_UNIQUE_TO_CLIENT = "PolicyID must be unique to a client.";
        public const string VALIDATION_ERROR_AT_LINE = "Validation Error processing input at line";
        public const string VALIDATION_ERROR_POLICY_FIELDS_COUNT = "Policy data field count mismatch found.";
        public const string VALIDATION_ERROR_MANDATORY_FIELD = "is a manadatory field";

        public const string SUCCESS_AT_LINE = "Success processing input at line";

        // Error Codes
        public const string ERROR_101 = "ERROR_101"; // File not found
        public const string ERROR_102 = "ERROR_102"; // Directory does not exist
        public const string ERROR_103 = "ERROR_103"; // Argument error
        public const string ERROR_104 = "ERROR_104"; // File I/O error
        public const string ERROR_105 = "ERROR_105"; // File path too long
        public const string ERROR_106 = "ERROR_106"; //Unknown error


        // Exception messages
        public const string EXCEPTION_OCCURRED = "Exception occured in";
        public const string SERVICE_LAYER = "Service layer";
    }
}
