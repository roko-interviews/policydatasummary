﻿using Common.ServiceInterfaces;
using Infrastructure;
using Infrastructure.Exceptions;
using Infrastructure.Utilities;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace ConsoleApp
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                SimpleLogger.Log("Program started......");

                if (args.Length == 2)
                {
                    var services = new ServiceCollection();

                    // Configure DI container for services
                    services.AddDataProcessors();
                    services.AddDataImportersAndExporters(args[0], args[1]);
                    var serviceProvider = services.BuildServiceProvider();

                    // Resolve policy data processor instance from the container.
                    var policyDataProcessor = serviceProvider.GetService<IPolicyDataProcessor>();

                    policyDataProcessor.Run();
                }
                else
                {
                    SimpleLogger.LogWarning("This program cannot work without 2 command line args passed to it < i, e input file path, output file path>.");
                }

                SimpleLogger.Log("Program execution finished. Press any key to exit..........");
            }
            catch(ServiceLayerException ex)
            {
                SimpleLogger.LogError($"Error Message>> {ex.Message} ErrorCode {ex.ErrorDetails.ErrorCode}");
                SimpleLogger.LogError($"InnerException>> {ex.ErrorDetails.InnerException}", true);
            }
            catch (Exception ex) 
            {
                SimpleLogger.LogError("Program encountered an unknown error.");
                SimpleLogger.LogError($"Error details >> {ex.Message}", true);
            }
            Console.ReadKey();
        }
    }
}
