﻿using Common.StringConstants;
using System;
using System.Collections.Generic;

namespace Domain
{
    /// <summary>
    /// Domain class for policy data.
    /// </summary>
    public class PolicyData
    {
        #region Properties
        /// <summary>
        /// Gets the policy id.
        /// </summary>
        public string PolicyID { get; private set; }

        /// <summary>
        /// Gets the cliet id.
        /// </summary>
        public int? ClientID { get; private set; }

        /// <summary>
        /// Gets the country code.
        /// </summary>
        public string CountryCode { get; private set; }

        /// <summary>
        /// Gets the cost.
        /// </summary>
        public decimal? Cost { get; private set; }

        /// <summary>
        /// Gets the income.
        /// </summary>
        public decimal? Income { get; private set; }

        public IReadOnlyCollection<string> ValidationErrors { get; private set; }

        public static int NumberOfPolicyDataFieldsRequired = 5;

        public bool IsValidInitialization { get; set; } = false;

        #endregion

        #region Constructors

        /// <summary>
        /// Creates an instance of PolicyData object.
        /// </summary>
        /// <param name="policyID">Policy ID</param>
        /// <param name="clientID">Client ID</param>
        /// <param name="countryCode">Country Code</param>
        /// <param name="cost">Cost</param>
        /// <param name="income">Income</param>
        public PolicyData(string policyID, int clientID, string countryCode, decimal cost, decimal income)
        {
            InitializePolicyData(policyID, clientID, countryCode, cost, income);
        }

        /// <summary>
        /// Creates an instance of PolicyData object.
        /// </summary>
        /// <param name="csvLine">Policy data in csv format.</param>
        public PolicyData(string csvLine)
        {
            string[] policyDataFields = csvLine.Split(",", StringSplitOptions.None);

            if (PolicyData.NumberOfPolicyDataFieldsRequired == policyDataFields.Length)
            {
                int clientId;
                decimal cost;
                decimal income;

                PolicyID = policyDataFields[0];
                ClientID = int.TryParse(policyDataFields[1], out clientId) ? clientId : null;
                CountryCode = policyDataFields[2];
                Cost = decimal.TryParse(policyDataFields[3], out cost) ? cost : null;
                Income = decimal.TryParse(policyDataFields[4], out income) ? income : null;

                ValidatePolicyData();
            }
            else
            {
                ValidationErrors = new List<string>() { $"{StringRrsources.VALIDATION_ERROR_POLICY_FIELDS_COUNT}" }.AsReadOnly(); 
            }
        }

        #endregion

        #region Public Methods

        /// <summary>
        /// Validates this instance of policy data.
        /// </summary>
        /// <returns>Validations errors as list of string.</returns>
        public IReadOnlyCollection<string> ValidatePolicyData()
        {
            ValidationErrors = ValidatePolicyData(this.PolicyID, this.ClientID, this.CountryCode, this.Cost, this.Income);
            return ValidatePolicyData(this.PolicyID, this.ClientID, this.CountryCode, this.Cost, this.Income);
        }

        public bool Equals(PolicyData other)
        {
            return other != null
                && other.PolicyID.ToLower() == this.PolicyID.ToLower();
        }

        public override bool Equals(object obj)
        {
            return Equals(obj as PolicyData);
        }

        public override int GetHashCode()
        {
            return HashCode.Combine(PolicyID);
        }

        #endregion

        #region Private Methods
        private void InitializePolicyData(string policyID, int clientID, string countryCode, decimal cost, decimal income)
        {
            this.PolicyID = policyID;
            this.ClientID = clientID;
            this.CountryCode = countryCode;
            this.Cost = cost;
            this.Income = income;

            IsValidInitialization = true;
        }

        public static IReadOnlyCollection<string> ValidatePolicyData(string policyID, int? clientID, string countryCode, decimal? cost, decimal? income)
        {
            List<string> validationErrors = new List<string>();

            if (string.IsNullOrEmpty(policyID) || string.IsNullOrWhiteSpace(policyID))
            {
                validationErrors.Add($"{nameof(PolicyID)} {StringRrsources.VALIDATION_ERROR_MANDATORY_FIELD}");
            }

            if (!clientID.HasValue)
            {
                validationErrors.Add($"{nameof(ClientID)} {StringRrsources.VALIDATION_ERROR_MANDATORY_FIELD}");
            }

            if (string.IsNullOrEmpty(countryCode) || string.IsNullOrWhiteSpace(countryCode))
            {
                validationErrors.Add($"{nameof(CountryCode)} {StringRrsources.VALIDATION_ERROR_MANDATORY_FIELD}");
            }

            if (!cost.HasValue)
            {
                validationErrors.Add($"{nameof(Cost)} {StringRrsources.VALIDATION_ERROR_MANDATORY_FIELD}");
            }

            if (!income.HasValue)
            {
                validationErrors.Add($"{nameof(Income)} {StringRrsources.VALIDATION_ERROR_MANDATORY_FIELD}");
            }
            return validationErrors.AsReadOnly();
        }

        #endregion
    }
}
