﻿namespace Domain
{
    /// <summary>
    /// A contract class for policy data summary.
    /// </summary>
    public class PolicySummary
    {
        /// <summary>
        /// Gets or sets the country code.
        /// </summary>
        public string CountryCode { get; set; }

        /// <summary>
        /// Gets or sets the cost.
        /// </summary>
        public decimal TotalCost { get; set; }

        public string GetCsvContent()
        {
            return $"{CountryCode}, {TotalCost}";
        }

        public static string GetCsvHeader()
        {
            return $"{nameof(CountryCode)}, {nameof(TotalCost)}";
        }

    }
}
