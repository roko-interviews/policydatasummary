﻿using Common.ServiceInterfaces;

namespace Infrastructure.DataProcessors
{
    /// <summary>
    /// An abstract class for Data Processors.
    /// 
    /// Template Method design pattern has been employed, where sub classes will override/implement steps of the algorithm.
    /// </summary>
    /// <typeparam name="T1"></typeparam>
    /// <typeparam name="T2"></typeparam>
    public abstract class DataProcessorBase<T1, T2> : IPolicyDataProcessor
    {
        private readonly IDataImporter<T1> _dataImporter;
        private readonly IDataExporter<T2> _dataExporter;
        public T1 InputData { get; private set; }

        public T2 OutputData { get; protected set; }
        public DataProcessorBase(IDataImporter<T1> dataImporter, IDataExporter<T2> dataExporter)
        {
            this._dataImporter = dataImporter;
            this._dataExporter = dataExporter;
        }

        /// <summary>
        /// Defines and runs the steps for main algorithm.
        /// </summary>
        public void Run()
        {
            ImportData();
            ProcessData();
            ExportData();
        }

        protected virtual void ImportData()
        {
            InputData = _dataImporter.Import();
        }
        protected abstract void ProcessData();
        protected virtual void ExportData()
        {
            _dataExporter.Export(OutputData);
        }
    }
}
