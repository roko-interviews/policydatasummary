﻿using Common.ServiceInterfaces;
using Domain;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.DataProcessors
{
    public class PolicyDataProcessor : DataProcessorBase<ISet<PolicyData>, IOrderedEnumerable<PolicySummary>>
    {
        public PolicyDataProcessor(IDataImporter<ISet<PolicyData>> dataImporter, IDataExporter<IOrderedEnumerable<PolicySummary>> dataExporter)
            :base(dataImporter, dataExporter)
        {
        }

        protected override void ProcessData()
        {
            // Order is preserved in OutputData.
            OutputData = InputData.GroupBy(p => p.CountryCode, StringComparer.InvariantCultureIgnoreCase)
                .Select(g => new PolicySummary() { CountryCode = g.Key.ToLower(), TotalCost = g.Sum(p => p.Cost.Value) })
                .OrderBy(s => s.CountryCode);

        }
    }
}
