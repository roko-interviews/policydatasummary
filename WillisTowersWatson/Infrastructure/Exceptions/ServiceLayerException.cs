﻿using Common.Contracts;
using System;

namespace Infrastructure.Exceptions
{
    public class ServiceLayerException : Exception
    {
        /// <summary>
        /// Error details for the exception.
        /// </summary>
        public ErrorData ErrorDetails { get; set; }
        public ServiceLayerException(string message) : base(message)
        {

        }

        public ServiceLayerException() : base()
        {

        }
    }
}
