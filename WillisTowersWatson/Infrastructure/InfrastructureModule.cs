﻿using Common.ServiceInterfaces;
using Domain;
using Infrastructure.DataProcessors;
using Infrastructure.ServiceImpl;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure
{
    public static class InfrastructureModule
    {
        public static void AddDataProcessors(this IServiceCollection services)
        {
            services.AddScoped<IPolicyDataProcessor, PolicyDataProcessor>();
        }

        public static void AddDataImportersAndExporters(this IServiceCollection services, string inputFilePath, string outputFilePath)
        {
            services.AddScoped<IImporter>(x => new CsvImporterService(inputFilePath));
            services.AddScoped<IExporter>(x => new CsvExporterService(outputFilePath));

            services.AddScoped<IDataImporter<ISet<PolicyData>>, PolicyDataImporterService>();
            services.AddScoped<IDataExporter<IOrderedEnumerable<PolicySummary>>, PolicyDataExporterService>();
        }
    }
}
