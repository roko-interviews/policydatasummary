﻿using Common.Contracts;
using Common.ServiceInterfaces;
using Infrastructure.Exceptions;
using Infrastructure.Utilities;
using System;
using System.IO;

namespace Infrastructure.ServiceImpl
{
    public class CsvExporterService : IExporter
    {
        private readonly string _filePath;
        public CsvExporterService(string filePath)
        {
            this._filePath = filePath;
        }

        /// <summary>
        /// Exports to external system.
        /// </summary>
        /// <param name="policySummaryCsvContent"></param>
        public void ExportToExternalSystem(string policySummaryCsvContent)
        {
            try
            {
                File.WriteAllText(_filePath, policySummaryCsvContent);
                SimpleLogger.LogSuccess($"Output exported to {_filePath}.");
            }
            catch(Exception ex) when (ex is PathTooLongException || ex is DirectoryNotFoundException || 
            ex is ArgumentNullException || ex is ArgumentException || ex is IOException)
            {
                HandleException(policySummaryCsvContent, ex);
            }

            
        }

        private void HandleException(string policySummaryCsvContent, Exception ex)
        {
            SimpleLogger.LogError($"{nameof(CsvImporterService)} encountered {ex?.GetType().Name}. ErrorDetails {ex.Message}");
            string newFilePath = $"{Path.Combine(Environment.CurrentDirectory, Path.GetFileName(_filePath))}";
            SimpleLogger.Log($"Dumping the ouput file in current directory ({newFilePath}).");
            File.WriteAllText(newFilePath, policySummaryCsvContent);
            SimpleLogger.LogSuccess($"Output exported to {newFilePath}.");
        }

        private static void ThrowServiceLayerException(Exception ex, string errorCode)
        {
            SimpleLogger.LogError($"{nameof(CsvImporterService)} encountered {ex?.GetType().Name}. ErrorDetails {ex.Message}", true);
            throw new ServiceLayerException($"{ex.Message}")
            {
                ErrorDetails = new ErrorData() { ErrorCode = errorCode, InnerException = ex }
            };
        }

    }
}
