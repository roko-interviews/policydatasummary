﻿using Common.Contracts;
using Common.ServiceInterfaces;
using Common.StringConstants;
using Infrastructure.Exceptions;
using Infrastructure.Utilities;
using System;
using System.IO;

namespace Infrastructure.ServiceImpl
{
    public class CsvImporterService : IImporter
    {
        private readonly string _filePath;
        public CsvImporterService(string filePath)
        {
            this._filePath = filePath;
        }
        
        /// <summary>
        /// Imports from external system.
        /// </summary>
        /// <returns></returns>
        public string[] ImportFromExternalSystem()
        {
            string[] fileContent = null;

            try
            {
                SimpleLogger.Log($"Importing data from {_filePath}");
                fileContent = File.ReadAllLines(_filePath);
            }
            catch(FileNotFoundException ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_101);
            }
            catch (DirectoryNotFoundException ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_102);
            }
            catch (PathTooLongException ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_105);
            }
            catch (ArgumentNullException ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_103);
            }
            catch (ArgumentException ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_103);
            }
            catch (IOException ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_104);
            }
            catch (Exception ex)
            {
                ThrowServiceLayerException(ex, StringRrsources.ERROR_106);
            }

            return fileContent;
        }

        private static void ThrowServiceLayerException(Exception ex, string errorCode)
        {
            SimpleLogger.LogError($"{nameof(CsvImporterService)} encountered {ex?.GetType().Name}. ErrorDetails {ex.Message}", true);
            throw new ServiceLayerException($"{ex.Message}")
            {
                ErrorDetails = new ErrorData() { ErrorCode = errorCode, InnerException = ex }
            };
        }
    }
}
