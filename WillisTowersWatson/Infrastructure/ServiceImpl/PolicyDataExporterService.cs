﻿using Common.ServiceInterfaces;
using Domain;
using Infrastructure.Utilities;
using System;
using System.Linq;

namespace Infrastructure.ServiceImpl
{
    public class PolicyDataExporterService : IDataExporter<IOrderedEnumerable<PolicySummary>>
    {
        private readonly IExporter _exporter;
        public PolicyDataExporterService(IExporter exporter)
        {
            this._exporter = exporter;
        }

        /// <summary>
        /// Exports policy summary data.
        /// </summary>
        /// <param name="data"></param>
        public virtual void Export(IOrderedEnumerable<PolicySummary> data)
        {
            if (data.Count() > 0)
            {
                string policySummaryCsvContent = $"{PolicySummary.GetCsvHeader()}{Environment.NewLine}" 
                    + $"{string.Join($"{Environment.NewLine}", data.Select(s => s.GetCsvContent()))}";

                this._exporter.ExportToExternalSystem(policySummaryCsvContent);
            }
            else
            {
                SimpleLogger.LogWarning("Nothing to export.");
            }
        }
    }
}
