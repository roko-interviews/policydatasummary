﻿using Common.ServiceInterfaces;
using Common.StringConstants;
using Domain;
using Infrastructure.Utilities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Infrastructure.ServiceImpl
{
    public class PolicyDataImporterService : IDataImporter<ISet<PolicyData>>
    {
        private readonly IImporter _importer;

        public PolicyDataImporterService(IImporter importer)
        {
            this._importer = importer;
        }

        /// <summary>
        /// Imports policy data.
        /// </summary>
        /// <returns></returns>
        public ISet<PolicyData> Import()
        {
            var allCsvLines = _importer.ImportFromExternalSystem();

            // Using HashSet for performance and guarranty of uniqueness of its items.
            ISet<PolicyData> policyDataSet = new HashSet<PolicyData>();

            // Assumption: If the first line contains PolicyID string, then the first line is column header.
            bool doesColumnHeaderExist = allCsvLines.Length > 0 ? allCsvLines[0].Contains(nameof(PolicyData.PolicyID), StringComparison.OrdinalIgnoreCase) : false;

            for (int i = doesColumnHeaderExist ? 1 : 0; i < allCsvLines.Length; i++)
            {
                string errorLogs = string.Empty;
                PolicyData policy = new PolicyData(allCsvLines[i]);
                
                // All the required validation happens in PolicyData class, except the PolicyID uniqueness check, which must happen here.
                bool isPolicyValid = !policy.ValidationErrors.Any();
                
                bool isPolicyUnique = false;

                if (isPolicyValid)
                {
                    // If policy id is not unique HashSet will not add it to the collection.
                    isPolicyUnique = policyDataSet.Add(policy);

                    if (!isPolicyUnique)
                    {
                        errorLogs = $"{StringRrsources.VALIDATION_ERROR_AT_LINE} {i + 1} : {StringRrsources.VALIDATION_ERROR_POLICY_ID_MUST_BE_UNIQUE_TO_CLIENT}";
                    }
                }
                else
                {
                    errorLogs = $"{StringRrsources.VALIDATION_ERROR_AT_LINE} {i + 1} : {string.Join(",", policy.ValidationErrors)}";
                }

                if (errorLogs.Any())
                {
                    SimpleLogger.LogError(errorLogs);
                }
                else 
                {
                    SimpleLogger.LogSuccess($"{StringRrsources.SUCCESS_AT_LINE} {i + 1}");
                }
            }

            if (policyDataSet.Count > 0)
            {
                SimpleLogger.LogSuccess($"{policyDataSet.Count} data point(s) have been imported.");
            }
            else
            {
                SimpleLogger.LogWarning($"No data point has been imported. EitheR the input file is empty, or there are no valid data.");
            }

            return policyDataSet;
        }
    }
}
