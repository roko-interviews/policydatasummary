﻿using System;
using System.IO;

namespace Infrastructure.Utilities
{
    /// <summary>
    /// A simple class to log text to files.
    /// </summary>
    public class SimpleLogger
    {
        private static string fileName = string.Empty;
        static SimpleLogger()
        {
            fileName = $"Log_{DateTime.Now.ToString("yyyyMMddHHmmss")}.log";
        }

        /// <summary>
        /// Logs text message to the log file and console.
        /// </summary>
        /// <param name="logtext">Text to log</param>
        public static void Log(string logtext, bool fileLoggingOnly = false)
        {
            string logWithTimeStamp = $"{DateTime.Now} : {logtext} {Environment.NewLine}";

            File.AppendAllText(fileName, logWithTimeStamp);

            if (!fileLoggingOnly)
            {
                Console.WriteLine(logWithTimeStamp);
            }
        }

        /// <summary>
        /// Logs an error message to the log file and console.
        /// </summary>
        /// <param name="logtext"></param>
        public static void LogError(string logtext, bool fileLoggingOnly = false)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Log(logtext, fileLoggingOnly);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        /// <summary>
        /// Logs an warning message to the log file and console.
        /// </summary>
        /// <param name="logtext"></param>
        public static void LogWarning(string logtext, bool fileLoggingOnly = false)
        {
            Console.ForegroundColor = ConsoleColor.Yellow;
            Log(logtext, fileLoggingOnly);
            Console.ForegroundColor = ConsoleColor.Gray;
        }

        /// <summary>
        /// Logs an warning message to the log file and console.
        /// </summary>
        /// <param name="logtext"></param>
        public static void LogSuccess(string logtext, bool fileLoggingOnly = false)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Log(logtext, fileLoggingOnly);
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}
