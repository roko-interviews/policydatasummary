﻿using Common.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Mocks
{
    public class MockCsvExporterService : IExporter
    {
        public void ExportToExternalSystem(string policySummaryCsvContent)
        {
            TestContext.CsvOuput = policySummaryCsvContent;
        }
    }
}
