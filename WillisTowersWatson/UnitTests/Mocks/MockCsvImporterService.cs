﻿using Common.ServiceInterfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests.Mocks
{
    public class MockCsvImporterService : IImporter
    {
        public string[] ImportFromExternalSystem()
        {
            return TestContext.PolicyDataInputCsv.Split(Environment.NewLine);
        }
    }
}
