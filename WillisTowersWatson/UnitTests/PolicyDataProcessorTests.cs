using Common.ServiceInterfaces;
using Microsoft.Extensions.DependencyInjection;
using NUnit.Framework;

namespace UnitTests
{
    public class PolicyDataProcessorTests
    {
        [OneTimeSetUp]
        public void FixtureSetUp()
        {
        }
        [SetUp]
        public void Setup()
        {
        }

        [TearDown]
        public void CleanupAftertEachTest()
        {
            TestContext.CsvOuput = null;
        }

        [Test]
        public void TestGoldenPath_WithHeaders()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            string validCsvInput = @"PolicyID,ClientID,CountryCode,Cost,Income
123def45,68,de,3.43,5.13
4d5w62,73,fr,1.23,0
21q32lk,45,fr,34.33,-34
Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101";

            string expectedOutput = @"CountryCode, TotalCost
de, 3.43
fr, 35.66
gb, 99";

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOutput, TestContext.CsvOuput);
        }

        [Test]
        public void TestGoldenPath_WithoutHeader()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            string validCsvInput = @"123def45,68,de,3.43,5.13
4d5w62,73,fr,1.23,0
21q32lk,45,fr,34.33,-34
Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101";

            string expectedOutput = @"CountryCode, TotalCost
de, 3.43
fr, 35.66
gb, 99";

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOutput, TestContext.CsvOuput);
        }

        [Test]
        public void TestInvalidInputScenarios_EmptyInput()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            string validCsvInput = @"";

            
            string expectedOutput = null;

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOutput, TestContext.CsvOuput);
        }

        [Test]
        public void TestPolicyDataOrderingInSummary()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Input order- fr,uk,de
            string validCsvInput = @"4d5w62,73,fr,1.23,0
21q32lk,45,fr,34.33,-34
Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101
123def45,68,de,3.43,5.13";

            // Output order- de,fr,gb
            string expectedOrderedOutput = @"CountryCode, TotalCost
de, 3.43
fr, 35.66
gb, 99";

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }

        [Test]
        public void TestCountryCodeCaseInsensitivityInPolicySummaryGrouping()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Input order- fr,uk,de
            string validCsvInput = @"4d5w62,73,fr,1.23,0
21q32lk,45,fr,34.33,-34
123def45,68,DE,4.43,5.13
Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101
123def46,68,de,3.43,5.13";

            // Output order- de,fr,gb
            string expectedOrderedOutput = @"CountryCode, TotalCost
de, 7.86
fr, 35.66
gb, 99";

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }

        [Test]
        public void TestPolicyIdUniqueToCustomer()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Line 3 and 6 has same policy Id but 2 different customers
            // Line 3 and 7 has same policy Id and same customer
            string validCsvInput = @"4d5w62,73,fr,1.23,0
21q32lk,45,fr,34.33,-34
123def45,68,DE,4.43,5.13
Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101
123def45,78,gb,99,101
123def45,78,de,3.43,5.13";

            // Output order- de,fr,gb
            string expectedOrderedOutput = @"CountryCode, TotalCost
de, 4.43
fr, 35.66
gb, 99";

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }



        [Test]
        public void TestInvalidInputScenarios_Line1HasUnsupportedDataType()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Line 1 has unsupported data type for income
            string csvInput = @"4d5w62,73,fr,1.23,0xy
21q32lk,45,fr,34.33,-34
Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101
123def45,68,de,3.43,5.13";

            // Output cost for fr should be 34.43, as the first row will be excluded from processing.
            string expectedOrderedOutput = @"CountryCode, TotalCost
de, 3.43
fr, 34.43
gb, 99";

            TestContext.PolicyDataInputCsv = csvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }

        [Test]
        public void TestInvalidInputScenarios_Line2HasNoData()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Second line has no data
            string csvInput = @"4d5w62,73,fr,1.23,0

Lh782U,45,fr,0.1,0.1
PID_UK12,68,gb,99,101
123def45,68,de,3.43,5.13";

            // Output cost for fr should be 34.43, as the first row will be excluded from processing.
            string expectedOrderedOutput = @"CountryCode, TotalCost
de, 3.43
fr, 1.33
gb, 99";

            TestContext.PolicyDataInputCsv = csvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }

        [Test]
        public void TestInvalidInputScenarios_EveryLineHasInvalidData()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Second line has no data
            string csvInput = @"4d5w62,73,fr,1.23ty,0xyz

Lh782U,45,fr,0.1,0.1jj
,68,gb,99,101
123def45,68,,3.43,5.13bb";

            // Output cost for fr should be 34.43, as the first row will be excluded from processing.
            string expectedOrderedOutput = null;

            TestContext.PolicyDataInputCsv = csvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }

        [Test]
        public void TestInvalidInputScenarios_EveryLineExceptOneHasInvalidData()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Only line 5 (Country Au) has valid data.
            string csvInput = @"4d5w62,73,fr,1.23ty,0xyz

Lh782U,45,fr,0.1,0.1jj
,68,gb,99,101,
Dk365,100,Au,61.2, 200
,,,,
123def45,68,,3.43,5.13bb";

            // Output cost for fr should be 34.43, as the first row will be excluded from processing.
            string expectedOrderedOutput = @"CountryCode, TotalCost
au, 61.2";

            TestContext.PolicyDataInputCsv = csvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOrderedOutput, TestContext.CsvOuput);

        }

        [Test]
        public void TestInvalidInputScenarios_ExtraColumns()
        {
            var policyDataProcessor = TestContext.ServiceProvider.GetService<IPolicyDataProcessor>();

            // Line 2, 5 has extra columns
            string validCsvInput = @"PolicyID,ClientID,CountryCode,Cost,Income
123def45,68,de,3.43,5.13,77, cc
4d5w62,73,fr,1.23,0
21q32lk,45,fr,34.33,-34
Lh782U,45,fr,0.1,0.1, bb, 8
PID_UK12,68,gb,99,101";

            string expectedOutput = @"CountryCode, TotalCost
fr, 35.56
gb, 99";

            TestContext.PolicyDataInputCsv = validCsvInput;

            policyDataProcessor.Run();

            Assert.AreEqual(expectedOutput, TestContext.CsvOuput);
        }

    }
}