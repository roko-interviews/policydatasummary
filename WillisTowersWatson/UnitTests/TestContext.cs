﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnitTests
{
    public class TestContext
    {
        static TestContext()
        {
            var services = new ServiceCollection();

            ServiceCollection = services;
            TestModule.AddTestModule(services);
            var serviceProvider = services.BuildServiceProvider();
            ServiceProvider = serviceProvider;
        }
        public static string CsvOuput { get; set; }

        public static string PolicyDataInputCsv { get; set; }

        public static IServiceCollection ServiceCollection { get; set; }

        public static IServiceProvider ServiceProvider { get; set; }
    }
}
