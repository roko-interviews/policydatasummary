﻿using Common.ServiceInterfaces;
using Domain;
using Infrastructure.DataProcessors;
using Infrastructure.ServiceImpl;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnitTests.Mocks;

namespace UnitTests
{
    public static class TestModule
    {
        public static void AddTestModule(this IServiceCollection services)
        {
            services.AddScoped<IPolicyDataProcessor, PolicyDataProcessor>();
            services.AddScoped<IImporter>(x => new MockCsvImporterService());
            services.AddScoped<IExporter>(x => new MockCsvExporterService());

            services.AddScoped<IDataImporter<ISet<PolicyData>>, PolicyDataImporterService>();
            services.AddScoped<IDataExporter<IOrderedEnumerable<PolicySummary>>, PolicyDataExporterService>();

        }
    }
}
